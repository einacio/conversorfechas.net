﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace conversorFechas.net {
    public partial class Form1 : Form {
        DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, 0).ToLocalTime();
        public Form1() {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e) {
            Ocultar();
        }

        private void notifyIcon1_DoubleClick(object sender, EventArgs e) {
            Mostrar();
        }

        private void Form1_VisibleChanged(object sender, EventArgs e) {
            if (this.Visible) {
                mostrarToolStripMenuItem.Text = "Ocultar";
            } else {
                mostrarToolStripMenuItem.Text = "Mostrar";
            }
        }

        private void mostrarToolStripMenuItem_Click(object sender, EventArgs e) {
            if (this.Visible) {
                Ocultar();
            } else {
                Mostrar();
            }
        }

        private void cerrarToolStripMenuItem_Click(object sender, EventArgs e) {
            Application.Exit();
        }
        
        private void Ocultar() {
            this.Hide();
        }
        private void Mostrar() {
            if (!this.Visible) {
                this.Show();
            }
            this.Activate();
        }

        public DateTime DateFromTimestamp(int timestamp) {
            return epoch.AddSeconds(timestamp);
        }

        public int TimestampFromDate(DateTime date) {
            return (int) Math.Floor((date - epoch).TotalSeconds);
        }

        private void Form1_Load(object sender, EventArgs e) {
            Rectangle r = Screen.GetWorkingArea(this);
            Point p = new Point();
            p.X = r.Width - this.Width - 10;
            p.Y = r.Height - this.Height - 10;
            this.Location = p;

            setNow();
        }

        private void dtp_ValueChanged(object sender, EventArgs e) {
            txt.Text = TimestampFromDate(dtp.Value).ToString();
        }

        private void txt_TextChanged(object sender, EventArgs e) {
            try {
                dtp.Value = DateFromTimestamp(int.Parse(txt.Text));
            } catch {
                
            }
            
        }

        private void setNow() {
            dtp.Value = DateTime.Now;
            txt.Text = TimestampFromDate(dtp.Value).ToString();
        }

        private void nowToolStripMenuItem_Click(object sender, EventArgs e) {
            setNow();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e) {
            if (e.CloseReason == CloseReason.UserClosing) {
                e.Cancel = true;
                Ocultar();
            }
        }

        private void desdeElToolStripMenuItem_Click(object sender, EventArgs e) {
            if (Clipboard.ContainsText()) {
                string t = Clipboard.GetText();
                if (t.Length > 10) {
                    t = t.Substring(0, 10);
                }
                txt.Text = t;
                txt_TextChanged(txt, e);
                this.Activate();
                txt.Focus();
            }
        }
    }
}
